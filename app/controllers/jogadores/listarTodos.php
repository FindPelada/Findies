<?php
//Agradeço a DEUS pelo dom do conhecimento

/**
*@apiGrupo Jogadores
*@apiRota GET /api/jogadores
*@apiDescricao Listar todos os jogadores
*@apiSaidaErro  [ {"connection":false} , {"persistence":false} ]
*@apiSaidaSucesso [ { "id" : "1", "nome" : "usuario1", "sobrenome" : "sobrenome1", "apelido" : "apelido1" , "email" : "email1@email.com", "sexo" : "F", "nascimento" : "10/12/2016" }, { "id" : "2", "nome" : "usuario2", "sobrenome" : "sobrenome2", "apelido" : "apelido2" , "email" : "email2@email.com", "sexo" : "M", "nascimento" : "11/12/2016" } ]
*/

if(!@include_once(ROOT_DIR."/connection/conexaoUsuario.php")) {
  echo '{"connection":false}'; exit(0);
}

if(!@include_once(ROOT_DIR."/persistence/jogadores/listarTodosJogadores.php")) {
    echo '{"persistence":false}'; exit(0);
}

echo json_encode($retorno, JSON_PRETTY_PRINT);

?>
