<?php
//Agradeço a DEUS pelo dom do conhecimento

/**
*@apiGrupo Times
*@apiRota POST /api/times/new
*@apiDescricao Cadastrar times
*@apiEntrada idAdmin2 id Opcional -Segundo administrador do time
*@apiEntrada idAdmin3 id Opcional -Terceiro administrador do time
*@apiEntrada nomeTime String[100] Sim -Nome do time
*@apiEntrada mascote String[20] Sim -Mascote do time
*@apiEntrada cores Array Opcional -Cores de representação do time
*@apiEntrada estadio String[50] Opcional -Estadio do time
*@apiEntrada escudo Imagem Opcional -Imagem do time
*@apiSaidaErro { "status": false }
*@apiSaidaSucesso { "status": true }
*/
include_once(ROOT_DIR."/connection/conexaoUsuario.php");
include_once(ROOT_DIR."/validation/validarTime.php");

if (empty($retornoArrayError)) {

  include_once(ROOT_DIR."/persistence/times/cadastrarTime.php");
}

 ?>
