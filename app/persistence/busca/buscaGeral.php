<?php

$dados = $url[2];

$retornoGeral = array();
$retornoJogadores = array();
$retornoTimes = array();
$retornoPartidas = array();
$retornoLocais = array();

if (strlen($url[2]) >= 3) {
$resultadoJogadores = mysqli_query($banco, "SELECT DISTINCT idJogador, nome_jogador, apelido_jogador, email FROM jogador WHERE ((nome_jogador LIKE '%".$dados."%') OR (apelido_jogador LIKE '%".$dados."%') OR (email LIKE '%".$dados."%'))");

while ($row = mysqli_fetch_array($resultadoJogadores)) {
  $objetoJogadores['idJogador'] = $row['idJogador'];
  $objetoJogadores['nome'] = $row['nome_jogador'];
  $objetoJogadores['apelido'] = $row['apelido_jogador'];
  $objetoJogadores['email'] = $row['email'];

  array_push($retornoJogadores, $objetoJogadores);
}
$resultadoTime = mysqli_query($banco, "SELECT DISTINCT idTime, nome, mascote, estadio FROM `time` WHERE ((nome LIKE '%".$dados."%') OR (mascote LIKE '%".$dados."%') OR (estadio LIKE '%".$dados."%'))");

while ($row = mysqli_fetch_array($resultadoTime)) {
  $objetoTime['idTime'] = $row['idTime'];
  $objetoTime['nome'] = $row['nome'];
  $objetoTime['mascote'] = $row['mascote'];
  $objetoTime['estadio'] = $row['estadio'];

  array_push($retornoTimes, $objetoTime);

}


$resultadoPartida = mysqli_query($banco, "SELECT DISTINCT idPelada, nome_pelada, inicio, fim, local FROM pelada WHERE ((nome_pelada LIKE '%".$dados."%') OR (local LIKE '%".$dados."%'))");

while ($row = mysqli_fetch_array($resultadoPartida)) {
  $objetoPartida['idPartida'] = $row['idPelada'];
  $objetoPartida['nome'] = $row['nome_pelada'];
  $objetoPartida['hora_de_inicio'] = $row['inicio'];
  $objetoPartida['hora_de_termino'] = $row['fim'];
  $objetoPartida['local'] = $row['local'];

  array_push($retornoPartidas, $objetoPartida);

}

$resultadoLocais = mysqli_query($banco, "SELECT idLocal, nome_local, tipo_local FROM local WHERE ((nome_local LIKE '%".$dados."%') OR (tipo_local LIKE '%".$dados."%'))");

while ($row = mysqli_fetch_array($resultadoLocais)) {
  $objetoLocais['idLocal'] = $row['idLocal'];
  $objetoLocais['nome'] = $row['nome_local'];
  $objetoLocais['tipo'] = $row['tipo_local'];

  array_push($retornoLocais, $objetoLocais);

}
array_push($retornoGeral, $retornoJogadores, $retornoTimes, $retornoPartidas, $retornoLocais);
}
 ?>
