<?php

$arrayRetorno;
$statusValidation = true;

$comandoSelectAdmTime = mysqli_query($banco, "SELECT `adm_01` FROM `time` WHERE `idTime` = '$idTime'");

$admTime = mysqli_fetch_array($comandoSelectAdmTime)[0];


///////////CASO SO EXISTA 1 JOGADOR NO TIME

$selecionandoQtdJogadores = mysqli_query($banco, "SELECT COUNT(*) qtd FROM `jogador_has_time` WHERE `idTime` = '$idTime'");
$quantJogadores = mysqli_fetch_array($selecionandoQtdJogadores)[0];

///////////////////////////////////////////

if ($quantJogadores < 2) {
	$comandoDeleteTime = mysqli_query($banco, "DELETE FROM `time` WHERE `idTime` = '$idTime'");
	if ($comandoDeleteTime == 1) {
		$arrayRetorno['statusDeleteTime'] = $comandoDeleteTime;
	} else {
		$arrayRetorno['statusDeleteTime'] = false;
		$statusValidation = false;
	}
	$comandoDeleteTimeUnicoJogador = mysqli_query($banco, "DELETE FROM `jogador_has_time` WHERE `idTime` = '$idTime' AND `idJogador` = '$idJogador'");
	if ($comandoDeleteTimeUnicoJogador == 1) {
		$arrayRetorno['statusDeleteTimeUnicoJogador'] = $comandoDeleteTimeUnicoJogador;
	} else {
		$arrayRetorno['statusDeleteTimeUnicoJogador'] = false;
		$statusValidation = false;
	}
} elseif ($idJogador == $admTime) {
	$selecionarProxAdm = mysqli_query($banco, "SELECT `idJogador` FROM `jogador_has_time` WHERE `idTime` = '$idTime'");

	while ($row = mysqli_fetch_array($selecionarProxAdm)) {
		if ($row[0] == $admTime) {
			continue;
		} else {
			$novoAdm = $row[0];
			$comandoUpdateAdm = mysqli_query($banco, "UPDATE `time` SET `adm_01` = '$novoAdm' WHERE `idTime` = '$idTime'");
			if ($comandoUpdateAdm == 1) {
				$arrayRetorno['statusAlterarAdm'] = $comandoUpdateAdm;
			} else {
				$arrayRetorno['statusAlterarAdm'] = false;
				$statusValidation = false;
			}

			$comandoDeleteAdm = mysqli_query($banco, "DELETE FROM jogador_has_time WHERE `idTime` = '$idTime' AND `idJogador` = '$admTime'");
			if ($comandoDeleteAdm == 1) {
				$arrayRetorno['statusDeleteAdm'] = $comandoDeleteAdm;
			} else {
				$arrayRetorno['statusDeleteAdm'] = false;
				$statusValidation = false;
			}
			break;
		}
	}
} else {
	$comandoDeleteJogador = mysqli_query($banco, "DELETE FROM jogador_has_time WHERE `idTime` = '$idTime' AND `idJogador` = '$idJogador'");
	if ($comandoDeleteJogador == 1) {
		$arrayRetorno['statusDeleteJogador'] = $comandoDeleteJogador;
	} else {
		$arrayRetorno['statusDeleteJogador'] = false;
		$statusValidation = false;
	}
}

mysqli_close($banco);

if ($statusValidation == true) {
	echo json_encode(array($arrayRetorno));
}


  ?>