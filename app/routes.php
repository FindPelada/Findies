<?php
//Agradeço a DEUS pelo dom do conhecimento

header("Access-Control-Allow-Origin: *");
header("Content-type: application/json");
header("Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: X-Requested-With, Accept, Authorization, Content-type, Token");

DEFINE("ROOT_DIR", __DIR__);

$url = array_splice(preg_split("/\//", $_SERVER['REQUEST_URI']), 1);
$tamanhoURL = count($url);
//Definicao do methodo GET
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	if ($tamanhoURL > 1 && $url[1] == 'jogadores')
		switch ($_SERVER['REQUEST_URI']) {
			case '/api/jogadores':
				include ROOT_DIR.'/controllers/jogadores/listarTodos.php';
				break;
			case '/api/jogadores/'. preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI']):
				include ROOT_DIR.'/controllers/jogadores/capturarJogadorPorID.php';
				break;
			case '/api/jogadores/'. preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI']).'/amigos':
				include ROOT_DIR.'/controllers/jogadores/listarAmigosDoJogador.php';
				break;
			case '/api/jogadores/nome/' . preg_replace("/[^A-Za-zá-üÁ-Ü0-9]/", "", $tamanhoURL > 3 ? $url[3] : ''):
				include ROOT_DIR.'/controllers/jogadores/capturarJogadorPorNome.php';
				break;
			case '/api/jogadores/apelido/'. preg_replace("/[^A-Za-zá-üÁ-Ü0-9]/", "", $tamanhoURL > 3 ? $url[3] : ''):
				include ROOT_DIR.'/controllers/jogadores/capturarJogadorPorApelido.php';
				break;
			case '/api/jogadores/amigo/'. preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI']):
				include ROOT_DIR.'/controllers/jogadores/capturarAmigoPeloId.php';
				break;
			default:
				return json_encode(array('error' => 404));
		}
	else if ($tamanhoURL > 1 && $url[1] == 'times')
	switch ($_SERVER['REQUEST_URI']) {
		case '/api/times':
			include ROOT_DIR.'/controllers/times/listarTodosTimes.php';
			break;
		case '/api/times/'. preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI']):
			include ROOT_DIR.'/controllers/times/listarMeusTimes.php';
			break;
		case '/api/times/'. preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI']).'/jogadores':
			include ROOT_DIR.'/controllers/times/listarJogadoresDoTime.php';
			break;
		default:
			return json_encode(array('error' => 404));
		}
	else if ($tamanhoURL > 1 && $url[1] == 'locais')
	switch ($_SERVER['REQUEST_URI']) {
		case '/api/locais':
			include ROOT_DIR.'/controllers/locais/listarLocal.php';
			break;
		default:
			return json_encode(array('error' => 404));
	}
	else if ($tamanhoURL > 1 && $url[1] == 'partidas')
	switch ($_SERVER['REQUEST_URI']) {
		case '/api/partidas':
			include ROOT_DIR.'/controllers/partidas/listarPartidas.php';
			break;
		case '/api/partidas/'.preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI']):
			include ROOT_DIR.'/controllers/partidas/listarMinhasPartidas.php';
			break;
		case '/api/partidas/id/'.preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI']):
			include ROOT_DIR.'/controllers/partidas/listarPartidasPorId.php';
			break;
		default:
			return json_encode(array('error' => 404));
	} else if ($tamanhoURL > 1 && $url[1] == 'busca') {
		switch ($_SERVER['REQUEST_URI']) {
			case '/api/busca/'.preg_replace("/[^A-Za-zá-üÁ-Ü0-9]/", "", $tamanhoURL > 2 ? $url[2] : ''):
				include ROOT_DIR.'/controllers/busca/buscaGeral.php';
				break;
			case '/api/busca/jogadores/'.preg_replace("/[^A-Za-zá-üÁ-Ü0-9]/", "", $tamanhoURL > 3 ? $url[3] : ''):
				include ROOT_DIR.'/controllers/busca/buscaJogadores.php';
				break;
			case '/api/busca/times/'.preg_replace("/[^A-Za-zá-üÁ-Ü0-9]/", "", $tamanhoURL > 3 ? $url[3] : ''):
				include ROOT_DIR.'/controllers/busca/buscaTimes.php';
				break;
			case '/api/busca/partidas/'.preg_replace("/[^A-Za-zá-üÁ-Ü0-9]/", "", $tamanhoURL > 3 ? $url[3] : ''):
				include ROOT_DIR.'/controllers/busca/buscaPartidas.php';
				break;
			case '/api/busca/locais/'.preg_replace("/[^A-Za-zá-üÁ-Ü0-9]/", "", $tamanhoURL > 3 ? $url[3] : ''):
				include ROOT_DIR.'/controllers/busca/buscaLocais.php';
				break;
			default:
				return json_encode(array('error' => 404));
		}
	} elseif ($tamanhoURL > 1 && $url[1] == 'email') {
		switch ($_SERVER['REQUEST_URI']) {
			case '/api/email/'.preg_replace("/[^a-zA-Z0-9.@]*$/", "", $tamanhoURL > 2 ? $url[2] : ''):
				include ROOT_DIR.'/controllers/email/verificarEmail.php';
				break;
			default:
				return json_encode(array('error' => 404));
		}
	} elseif ($tamanhoURL > 1 && $url[1] == 'doc') {
		header("Location: /Findies/doc");
		exit(0);
	}
	else {
		echo json_encode(array('error' => 404));
		exit(0);
	}
}

//Definicao do methodo POST
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if ($url[1] == 'jogadores') {
		switch ($_SERVER['REQUEST_URI']) {
		case '/api/jogadores/new':
			include ROOT_DIR.'/controllers/jogadores/cadastro.php';
			break;
		case '/api/jogadores/login':
			include ROOT_DIR.'/controllers/jogadores/iniciarSessao.php';
			break;
		case '/api/jogadores/amigos/'.preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI']).'/new':
			include ROOT_DIR.'/controllers/jogadores/adicionarAmigos.php';
			break;
			default:
			return json_encode(array('error' => 404));
		}
	}
	else if ($url[1] == 'times') {
		switch ($_SERVER['REQUEST_URI']) {
		case '/api/times/new':
			include ROOT_DIR.'/controllers/times/cadastrarTime.php';
			break;
		case '/api/times/sair':
			include ROOT_DIR.'/controllers/times/sairTime.php';
			break;
		case '/api/times/amigos/'.preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI']).'/new':
			include ROOT_DIR.'/controllers/times/adicionarAmigosTime.php';
			break;
		default:
			return json_encode(array('error' => 404));
		}
	}
	else if ($url[1] == 'locais') {
		switch ($_SERVER['REQUEST_URI']) {
			case '/api/locais/new':
				include ROOT_DIR.'/controllers/locais/cadastrarLocal.php';
				break;
			default:
				return json_encode(array('error' => 404));
		}
	}
	else if ($url[1] == 'partidas') {
		switch ($_SERVER['REQUEST_URI']) {
			case '/api/partidas/new':
				include ROOT_DIR.'/controllers/partidas/cadastrarPartida.php';
				break;
			default:
				return json_encode(array('error' => 404));
		}
	}
	else
		return json_encode(array('errors' => 404));
}

if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
	if ($url[1] == 'jogadores') {
		switch ($_SERVER['REQUEST_URI']) {
		case '/api/jogadores/'.preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI']).'/delete':
			include ROOT_DIR.'/controllers/jogadores/deletarJogadorPorID.php';
			break;
		case '/api/jogadores/amigos/'.preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI']).'/delete':
			include ROOT_DIR.'/controllers/jogadores/deletarAmigos.php';
			break;
		default:
			return json_encode(array('error' => 404));
		}
	}
	else if ($url[1] == 'times') {
		switch ($_SERVER['REQUEST_URI']) {
		case '/api/times/'.preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI']).'/delete':
			include ROOT_DIR.'/controllers/times/deletarTime.php';
			break;
		case '/api/times/amigos/'.preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI']).'/delete':
			include ROOT_DIR.'/controllers/times/deletarAmigosTime.php';
			break;
		default:
			return json_encode(array('error' => 404));
		}
	}
	else if ($url[1] == 'locais'){
		switch ($_SERVER['REQUEST_URI']) {
			case '/api/locais/'. preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI']).'/delete':
				include ROOT_DIR.'/controllers/locais/deletarLocalPorId.php';
				break;
			default:
				return json_encode(array('error' => 404));
		}
	}
	else if ($url[1] == 'partidas') {
		switch ($_SERVER['REQUEST_URI']) {
			case '/api/partidas/'.preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI']).'/delete':
				include ROOT_DIR.'/controllers/partidas/deletarPartida.php';
				break;
			default:
				return json_encode(array('error' => 404));
		}
	}
	else {
		return json_encode(array('error' => 404));
	}
}

	if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
		if ($url[1] == 'jogadores') {
			switch ($_SERVER['REQUEST_URI']) {
				case '/api/jogadores/'.preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI']).'/update':
					include ROOT_DIR.'/controllers/jogadores/atualizarJogadorPorID.php';
					break;
				default:
					return json_encode(array('error' => 404));
			}
		} elseif ($url[1] == 'times') {
			switch ($_SERVER['REQUEST_URI']) {
				case '/api/times/'.preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI']).'/update':
					include ROOT_DIR.'/controllers/times/atualizarTime.php';
					break;
				default:
					return json_encode(array('error' => 404));
			}
		} elseif ($url[1] == 'partidas') {
			switch ($_SERVER['REQUEST_URI']) {
				case '/api/partidas/'.preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI']).'/update':
					include ROOT_DIR.'/controllers/partidas/atualizarPartida.php';
					break;
				default:
					return json_encode(array('error' => 404));
					break;
			}
		} elseif ($url[1] == 'locais') {
			switch ($_SERVER['REQUEST_URI']) {
				case '/api/locais/'.preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI'].'/update'):
					include ROOT_DIR.'/controllers/locais/AtualizarLocal.php';
					break;
				default:
					return json_encode(array('error' => 404));
					break;
			}
		} else {
			return json_encode(array('error' => 404));
		}
	}

?>
