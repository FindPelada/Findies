<?php

$objeto = json_decode(file_get_contents("php://input")) or die('{"json": false}');

$pegandoToken = apache_request_headers();

$token = $pegandoToken['Token'];

if (empty($token)) {
  die('{"statusToken": false}');
}

$comandoSelect = mysqli_query($banco, "SELECT `idJogador` FROM `infoToken` WHERE `token` = '$token'");

$idJogador = mysqli_fetch_array($comandoSelect)[0];

if (!preg_match('/^[0-9]+$/', $idJogador) or empty($idJogador)) {
  die('{"statusIdJogador": false}');
}

$retornoArrayError;

$nome = $objeto->nome_jogador;
$apelido = $objeto->apelido_jogador;
$data = $objeto->data_nascimento;
$email = $objeto->email;
$sexo = $objeto->sexo;
$sobrenome = $objeto->sobrenome_jogador;
$esporte = $objeto->esporte;
$data =implode('-', array_reverse(explode('/', $data)));
$dat = explode("-","$data");


$y = $dat[0];
$m = $dat[1];
$d = $dat[2];

if (!preg_match('/^[a-zA-Zá-üÁ-Ü0-9- ]+$/', $nome) or empty($nome)) {
	$retornoArrayError['statusNome'] = false;
}

if (!preg_match('/^[a-zA-Zá-üÁ-Ü- ]+$/', $esporte) or empty($esporte)) {
	$retornoArrayError['statusEsporte'] = false;
}

//if (!preg_match('/^[a-zA-Z- ]+$/', $apelido) or empty($apelido)) {
//	$retornoArrayError['statusApelido'] = false;
//}

if(!checkdate($m,$d, $y) or empty($data))
{
	$retornoArrayError['statusData'] = false;
}

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	$retornoArrayError['statusEmail'] = false;
}

//if (!preg_match('/^[a-zA-Z- ]+$/', $sexo) or empty($sexo)) {
//	$retornoArrayError['statusSexo'] = false;
//}

if (!preg_match('/^[a-zA-Zá-üÁ-Ü0-9- ]+$/', $sobrenome) or empty($sobrenome)) {
	$retornoArrayError['statusSobreNome'] = false;
}

if (!empty($retornoArrayError)) {
	die(json_encode(array($retornoArrayError)));
}

  ?>
