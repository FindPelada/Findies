<?php

$objeto = json_decode(file_get_contents("php://input")) or die('{"json": false}');

$pegandoToken = apache_request_headers();

$token = $pegandoToken['Token'];

if (empty($token)) {
  die('{"statusToken": false}');
}

$comandoSelect = mysqli_query($banco, "SELECT `idJogador` FROM `infoToken` WHERE `token` = '$token'");

$idJogador = mysqli_fetch_array($comandoSelect)[0];

if (!preg_match('/^[0-9]+$/', $idJogador) or empty($idJogador)) {
  die('{"statusIdJogador": false}');
}

$retornoArrayError;

$visibilidade = $objeto->visibilidade;
$frequencia = $objeto->frequencia;
$diasFrequencia = $objeto->diasFrequencia;
$horario = $objeto->inicio;
$jogadores = $objeto->jogadores;
$adm_partida = $idJogador;
$local = $objeto->local;
$data = $objeto->data;

foreach ($jogadores as $value) {
  if (!preg_match('/^[0-9]+$/', $value->idJogador) or empty($value->idJogador)) {
    die('{"statusJogadores": false}');
  }
}


if ($frequencia == "Constante") {

// VALIDANDO OS DIAS DA SEMANA
foreach ($diasFrequencia as $value) {
  if (!preg_match('/^[\w\D]+$/', $value)) {
    die('{"statusDiasFrequencia": false}');
  }
}

foreach ($diasFrequencia as $value) {
  switch (utf8_encode($value)) {
   case 'Segunda':
     $segunda = 'Segunda';
     break;
   case 'Terca':
     $terca = 'Terca';
     break;
   case 'Quarta':
     $quarta = 'Quarta';
     break;
   case 'Quinta':
     $quinta = 'Quinta';
     break;
   case 'Sexta':
     $sexta = 'Sexta';
     break;
   case 'Sabado':
     $sabado = 'Sábado';
     break;
   case 'Domingo':
     $domingo = 'Domingo';
     break;
   default:
     echo "Erro";
     break;
}

  }

} else if ($frequencia == "Única") {
  if (!preg_match('/^[A-Za-zá-üÁ-Ü]+$/', $frequencia) or empty($frequencia)) {
    $retornoArrayError['statusFrequencia'] = false;
  }
}

// VALIDANDO HORARIOS
/*if (!preg_match('/^[0-9:]+$/', $horario) or empty($horario)) {
  $retornoArrayError['statusHorario'] = false;
}*/

// VALIDANDO O ADM
if (!preg_match('/^[0-9]+$/', $adm_partida) or empty($adm_partida)) {
  $retornoArrayError['statusAdm'] = false;
}

// VALIDANDO O LOCAL

/*if (!preg_match('/^[A-Za-zá-üÁ-Ü0-9.\- ]+$/', $local) or empty($local)) {
  $retornoArrayError['statusLocal'] = false;
}*/

if (!empty($retornoArrayError)) {
	die(json_encode(array($retornoArrayError)));
}

 ?>
