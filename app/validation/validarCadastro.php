<?php
/*$statusValidation = true;
$objeto=file_get_contents("php://input");
$objeto=str_replace("{","", $objeto);
$objeto=str_replace("\"","", $objeto);
$objeto=str_replace("}","", $objeto);
$objeto=explode(",", $objeto);
foreach ($objeto as &$valor) {
	$valor=explode(":", $valor);
}
$objeto[5][1]=str_replace("\\n","", $objeto[5][1]);

$nome = $objeto[0][1];
$sobrenome = $objeto[1][1];
$data = $objeto[2][1];
$cep = $objeto[3][1];
$email = $objeto[4][1];
$senha = $objeto[5][1];
$data =implode('-', array_reverse(explode('/', $data)));
$dat = explode("-","$data");


$y = $dat[0];
$m = $dat[1];
$d = $dat[2];
*/

$objeto = json_decode(file_get_contents("php://input")) or die('{"json": false}');

$nome = $objeto->nome;
$sobrenome = $objeto->sobrenome;
$data = $objeto->date;
$cep = $objeto->cep;
$email = $objeto->emailcadastro;
$senha = $objeto->senhacadastro;
$data =implode('-', array_reverse(explode('/', $data)));
$dat = explode("-","$data");


$y = $dat[0];
$m = $dat[1];
$d = $dat[2];


$retornoArrayError;

if (!preg_match('/^[a-zA-Zá-üÁ-Ü0-9- ]+$/', $nome) or empty($nome)) {
	//echo json_encode(array('statusNome' => false ));
	//$statusValidation = false;
	$retornoArrayError['statusNome'] = false;
}

if (!preg_match('/^[a-zA-Zá-üÁ-Ü0-9- ]+$/', $sobrenome) or empty($sobrenome)) {
	//echo json_encode(array('statusSobrenome' => false ));
	//$statusValidation = false;
	$retornoArrayError['statusSobrenome'] = false;
}

if(!checkdate($m,$d, $y) or empty($data))
{
	//echo json_encode(array('statusData' => false ));
	//$statusValidation = false;
	$retornoArrayError['statusData'] = false;
}

if (!preg_match('/^[0-9-]+$/', $cep) or empty($cep)) {
	//echo json_encode(array('statusCep' => false ));
	//$statusValidation = false;
	$retornoArrayError['statusCep'] = false;
}


if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	//echo json_encode(array('statusEmail' => false ));
	//$statusValidation = false;
	$retornoArrayError['statusEmail'] = false;
}

if (empty($senha) or strlen($senha)<6) {
	//echo json_encode(array('statusSenha' => false ));
	//$statusValidation = false;
	$retornoArrayError['statusSenha'] = false;
}

if (!empty($retornoArrayError)) {
	die(json_encode(array($retornoArrayError)));
}

//não sei se é necessário fazer validação da senha, pois se a pessoa quiser usar caracteres especiais eu que não vou impedir ela de fazer isso. Antes de ser colocada no banco ela vai virar um hash, então só VALIDO SE O CAMPO TA VAZIO. VErificar se a senha é maior do que 6 caracteres é interessante.

?>
