<?php

$idTime = preg_replace('/[^0-9]/', "", $_SERVER['REQUEST_URI']);

$objeto = json_decode(file_get_contents("php://input")) or die('{"json": false}');

$pegandoToken = apache_request_headers();

$token = $pegandoToken['Token'];

if (empty($token)) {
  die('{"statusToken": false}');
}

$comandoSelect = mysqli_query($banco, "SELECT `idJogador` FROM `infoToken` WHERE `token` = '$token'");

$idJogador = mysqli_fetch_array($comandoSelect)[0];

if (!preg_match('/^[0-9]+$/', $idJogador) or empty($idJogador)) {
  die('{"statusIdJogador": false}');
}

$retornoArrayError;

$amigos = $objeto->amigos;

foreach ($amigos as $value) {
  if (!preg_match('/^[0-9]+$/', $value) or empty($value)) {
    die('{"statusId": false}');
  }
}

if (!preg_match('/^[0-9]+$/', $idTime) or empty($idTime)) {
  $retornoArrayError['statusIdTime'] = false;
}

if (!empty($retornoArrayError)) {
  die(json_encode(array($retornoArrayError)));
}

 ?>
