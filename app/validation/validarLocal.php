<?php

$objeto = json_decode(file_get_contents("php://input")) or die('{"json": false}');

$pegandoToken = apache_request_headers();

$token = $pegandoToken['Token'];

if (empty($token)) {
  die('{"statusToken": false}');
}

$comandoSelect = mysqli_query($banco, "SELECT `idJogador` FROM `infoToken` WHERE `token` = '$token'");

$idJogador = mysqli_fetch_array($comandoSelect)[0];

if (!preg_match('/^[0-9]+$/', $idJogador) or empty($idJogador)) {
  die('{"statusIdJogador": false}');
}

$retornoArrayError;

$nome_local = $objeto->nome_local;
$tipo_local = $objeto->tipo_local;

// VALIDAÇÃO DO NOME DO LOCAL
if (!preg_match('/^[a-zA-Zá-üÁ-Ü0-9- ]+$/', $nome_local) or empty($nome_local)) {
  $retornoArrayError['statusNomeLocal'] = false;
}

// VALIDAÇÃO DO TIPO DO LOCAL
if (!preg_match('/^[a-zA-Zá-üÁ-Ü0-9- ]+$/', $tipo_local) or empty($tipo_local)) {
  $retornoArrayError['statusTipoLocal'] = false;
}

if (!empty($retornoArrayError)) {
	die(json_encode(array($retornoArrayError)));
}

 ?>
