<?php

$objeto = json_decode(file_get_contents("php://input")) or die('{"json": false}');

$pegandoToken = apache_request_headers();

$token = $pegandoToken['Token'];

if (empty($token)) {
	die('{"statusToken": false}');
}

$comandoSelect = mysqli_query($banco, "SELECT `idJogador` FROM `infoToken` WHERE `token` = '$token'");

$idJogadorBanco = mysqli_fetch_array($comandoSelect)[0];

if (!preg_match('/^[0-9]+$/', $idJogadorBanco) or empty($idJogadorBanco)) {
  die('{"statusIdJogador": false}');
}

$idJogador = $objeto->idJogador;
if (!preg_match('/^[0-9]+$/', $idJogador) or empty($idJogador)) {
  die('{"statusIdJogador": false}');
}
$idTime = $objeto->idTime;
if (!preg_match('/^[0-9]+$/', $idTime) or empty($idTime)) {
  die('{"statusIdTime": false}');
}

if ($idJogadorBanco == $idJogador) {
	$comandoSelect = mysqli_query($banco, "SELECT `idTime` FROM `jogador_has_time` WHERE `idTime` = '$idTime' AND `idJogador` = '$idJogador'");
	$idTimeBanco = mysqli_fetch_array($comandoSelect)[0];
	if ($idTime != $idTimeBanco) {
		$retornoArrayError['idTime'] = false;
	}
} else {
	$retornoArrayError['idJogador'] = false;
}

if (!empty($retornoArrayError)) {
	die(json_encode(array($retornoArrayError)));
}

?>