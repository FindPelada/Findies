<?php

$statusValidationSessao = true;
$jsonRetorno;
$objeto = json_decode(file_get_contents("php://input"), true);

$emailSessao = $objeto['emailLogin'];
$senhaSessao = $objeto['senhaLogin'];

if (!filter_var($emailSessao, FILTER_VALIDATE_EMAIL)) {
		//echo json_encode(array('statusEmailLogin' => false ));
		$jsonRetorno['statusEmail'] = false;
		$statusValidationSessao = false;
}

if (empty($senhaSessao) or strlen($senhaSessao)<6) {
		//echo json_encode(array('statusSenha' => false ));
		$jsonRetorno['statusSenha'] = false;
		$statusValidationSessao = false;
}

if ($statusValidationSessao == false) {
		echo json_encode(array($jsonRetorno));
}
?>
