<?php

$obj = json_decode(file_get_contents("php://input")) or die('{"json": false}');

$pegandoToken = apache_request_headers();

$token = $pegandoToken['Token'];

if (empty($token)) {
  die('{"statusToken": false}');
}

$comandoSelect = mysqli_query($banco, "SELECT `idJogador` FROM `infoToken` WHERE `token` = '$token'");

$idJogador = mysqli_fetch_array($comandoSelect)[0];

if (!preg_match('/^[0-9]+$/', $idJogador) or empty($idJogador)) {
  die('{"statusIdJogador": false}');
}

$retornoArrayError;

$nome = $obj->nome;
$mascote = $obj->mascote;
$amigos = $obj->amigos;

foreach ($amigos as $value) {
	if (!preg_match('/^[0-9]+$/', $value) or empty($value)) {
		die('{"statusIdJogadores": false}');
	}
}

// CHECANDO A VALIDACAO DO NOME
if (!preg_match('/^[a-zA-Zá-üÁ-Ü0-9- ]+$/', $nome) or empty($nome)) {
	$retornoArrayError['statusNomeTime'] = false;
}

// CHECANDO A VALIDACAO DO MASCOTE
if (!preg_match('/^[a-zA-Z- ]+$/', $mascote) or empty($mascote)) {
	$retornoArrayError['statusMascote'] = false;
}

if (!empty($retornoArrayError)) {
	die(json_encode(array($retornoArrayError)));
}

 ?>
