app.controller('mainCtrl',["$scope", '$localStorage', 'jwtHelper', '$rootScope', function($scope, $localStorage, jwtHelper, $rootScope) {

  var getToken = function () {
    $rootScope.token = $localStorage.token;
  };
  var deleteToken = function(){
    delete $localStorage.token;
  };
  var setToken = function (token) {
    $localStorage.token = token;
  };
  getToken();

  var generalProcedures = function(token){
    var x = jwtHelper.decodeToken(token);
    $rootScope.user = {
      id: x.id,
      nome: x.nome,
      email: x.useremail
    };

  };
  generalProcedures($rootScope.token);

  $scope.logout = function(){
    deleteToken();
    location.href = 'http://localhost:8080/Findies/public';
  };

  console.log("mainCtrl carregado!");
}]);
