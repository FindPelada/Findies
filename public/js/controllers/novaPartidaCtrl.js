app.controller('novaPartidaCtrl',['$scope','$http', function($scope, $http) {
  $scope.titulo = "Nova Partida";
  $scope.showUnica = "Única";

  $http.get("http://localhost:8080/api/jogadores")
    .then(function(response) {
        $scope.amigos = response.data;
  });
  

var currentTime = new Date();
$scope.currentTime = currentTime;
$scope.month = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
$scope.monthShort = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
$scope.weekdaysFull = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];
$scope.weekdaysLetter = ['D', 'S', 'T', 'Q', 'Q', 'S','S'];
$scope.disable = [false];
$scope.today = 'Hoje';
$scope.clear = 'Limpar';
$scope.close = 'Fechar';
var days = 92;
$scope.minDate = (new Date($scope.currentTime.getTime())).toISOString();
$scope.maxDate = (new Date($scope.currentTime.getTime() + ( 1000 * 60 * 60 *24 * days ))).toISOString();


console.log("novaPartidaCtrl carregado!");
}]);
