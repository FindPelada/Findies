angular.module.exports = 'angular-jwt';
var app = angular.module('findies', ['ui.materialize', 'angular-jwt',"ngRoute", 'ngStorage']);
app.config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider) {

  // $httpProvider.defaults.useXDomain = true;
  // delete $httpProvider.defaults.headers.common['X-Requested-With'];

    $routeProvider
    .when("/", {
      templateUrl: "../views/home.html",
      controller: "homeCtrl"
    })
    .when("/amigos", {
      templateUrl: "../views/amigos.html",
      controller: "amigosCtrl"
    })
    .when("/novaPartida", {
      templateUrl: "../views/novaPartida.html",
      controller: "novaPartidaCtrl"
    })
    .when("/times", {
      templateUrl: "../views/times.html",
      controller: "timesCtrl"
    })
    .when("/novoTime", {
      templateUrl: "../views/novoTime.html",
      controller: "novoTimeCtrl"
    })
    .otherwise({
      templateUrl: "../views/home.html",
      controller: "homeCtrl"
    });
}]);
// app.all('/*', function (request, response, next) {
//     response.header("Access-Control-Allow-Origin", "*");
//     response.header("Access-Control-Allow-Headers", "X-Requested-With");
//     response.header("Access-Control-Allow-Methods", "GET, POST", "PUT", "DELETE");
//     next();
// });
