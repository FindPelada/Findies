$( document ).ready(function(){
  // ATIVA O MENU MOBILE
  $(".button-collapse").sideNav();
  // TABS
  $('ul.tabs').tabs();
  // SLIDER
  $('.slider').slider({
    full_width: true,
    full_height: true,
    indicators: false
  });
  
  // DATEPICKER
  // $('.datepicker').pickadate({
  //   format: 'dd/mm/yyyy',
  //   selectMonths: true,
  //   selectYears: 100,
  //   max: new Date(),
  //   closeOnSelect: true,
  // });
});
