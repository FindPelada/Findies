app.controller('amigosCtrl',["$scope", "$http", function($scope, $http) {
  $scope.titulo = "Amigos";
  $http.get(apiAdress + "jogadores")
    .then(function(response) {
        $scope.jogadores = response.data;
  });
  console.log("amigosCtrl carregado!");
}]);
