angular.module.exports = 'angular-jwt';
var app = angular.module('findies', ['ui.materialize', 'angular-jwt', 'ngStorage']);

app.controller('indexCtrl',['$scope', '$http', '$timeout', '$localStorage', 'jwtHelper', function($scope, $http, $timeout, $localStorage, jwtHelper) {

  $scope.divError = false;
  $scope.tempToken = null;
  // $scope.token = null;
  $scope.dadosEnviarLogin = {
    emailLogin: "",
    senhaLogin: ""
  };
  $scope.dadosEnviarCadastrar = {
  	nome:"",
  	sobrenome:"",
  	date: "",
  	cep:"",
  	emailcadastro:"",
  	senhacadastro: ""
  };

  var getToken = function () {
    $scope.token = $localStorage.token;
  };
  var deleteToken = function(){
    delete $localStorage.token;
  };
  var setToken = function (token) {
    $localStorage.token = token;
  };

  $scope.getAmigos = function(token) {
    var req = {
     method: 'GET',
     url: 'http://findies.esy.es/api/jogadores',
     headers: {
       'Content-Type': undefined,
       'token': token
     }
   };
    $http(req).then(function(response){
      $scope.amigos = response.data;
    });
  };

  $scope.sendPostLogin = function(dadosEnviarLogin) {
    var req = {
       method: 'POST',
       url: 'http://findies.esy.es/api/jogadores/login',
       headers: {
         'Content-Type': undefined
       },
       data: dadosEnviarLogin
    };
    $http(req).then(function(response){
      x = response.data;
      if (x.statusEmailouSenha === false) {
        $scope.divError = true;
        $scope.messageError = "Dados incorretos";
      }else {
        if (x.token !== null) {
          $scope.token = x.token;
          $scope.tokenDecoded = jwtHelper.decodeToken(x.token);
          setToken(x.token);
          location.href = 'http://localhost:8080/Findies/public/views/main.html';
          console.log(response.statusText);
        }
      }
    });
  };

  $scope.sendPostCadastrar = function(dadosEnviarCadastrar) {
    $scope.isEmail = "";
    $scope.isEmailExiste = "";
    $http.get("http://findies.esy.es/api/email/" + dadosEnviarCadastrar.emailcadastro)
      .then(function(response) {
          $scope.isEmail = response.data;
    });
    $scope.isEmailExiste = $scope.isEmail.existe;
    console.log($scope.isEmailExiste);
    if ($scope.isEmailExiste) {
      console.log("cima: " + $scope.isEmailExiste);
      // $scope.divError = true;
      // $scope.messaError = "Email já cadastrado!";
      console.log("Email já cadastrado!");
    }else {
      console.log("baixo: " + $scope.isEmailExiste);
      var req = {
         method: 'POST',
         url: 'http://findies.esy.es/api/jogadores/new',
         headers: {
           'Content-Type': undefined
         },
         data: dadosEnviarCadastrar
      };
      $http(req).then(function(response){
          console.log(response.statusText);
          // x = response.data;
          // if (x.token !== null) {
          //   $scope.token = x.token;
          //   console.log(response.statusText);
          // }else {
          //   if (!x.statusEmail) {
          //     console.log("Email ou senha incorretos");
          //   }
          // }
      });
    }
  };

  // var timeP = function(){

    var currentTime = new Date();
    $scope.currentTime = currentTime;
    $scope.month = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
    $scope.monthShort = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
    $scope.weekdaysFull = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];
    $scope.weekdaysLetter = ['D', 'S', 'T', 'Q', 'Q', 'S','S'];
    $scope.disable = [false];
    $scope.today = 'Hoje';
    $scope.clear = 'Limpar';
    $scope.close = 'Fechar';
    $scope.minDate = (new Date($scope.currentTime.getTime())).toISOString();
    $scope.maxDate = (new Date($scope.currentTime.getTime())).toISOString();
  // };
  // timeP();

}]);
