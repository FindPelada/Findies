
$( document ).ready(function(){
  // ATIVA O MENU MOBILE
  $(".button-collapse").sideNav();
  // ATIVA O DROPDOWN
  $('.collapsible').collapsible({
      accordion : true // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });
  $('.carousel').carousel();
  $('.modal-trigger').leanModal();
});
